package ru.test.myproject.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app.counter")
public class CounterServiceConfig {
    private String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
