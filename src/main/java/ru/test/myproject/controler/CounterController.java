package ru.test.myproject.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.test.myproject.service.CounterService;

@Controller
@RequestMapping("/counter")
public class CounterController {

    private CounterService _mainService;

    @Autowired
    public CounterController(CounterService counterService) {
        _mainService = counterService;
    }

    @RequestMapping(value = "/get/{name}")
    @GetMapping
    public ResponseEntity createCounter(@PathVariable(value = "name") String cntrName) {
        return _mainService.valueCounter(cntrName);
    }

    /*
    Можно было использовать  PostMapping ,
    Для удобства тестирования оставил GetMapping
    */
    @RequestMapping(value = "/create/{name}")
    @GetMapping
    public ResponseEntity getCounter(@PathVariable(value = "name") String cntrName) {
        return _mainService.createCounter(cntrName);
    }

    /*
    Можно было использовать  PutMapping ,
    Для удобства тестирования оставил GetMapping
     */
    @RequestMapping(value = "/increment/{name}")
    @GetMapping
    public ResponseEntity addValueToCounter(@PathVariable(value = "name") String cntrName,
                                            @RequestParam(value = "int", required = false) Integer number) {
        number = number == null ? 1 : number;
        return _mainService.addToCounter(cntrName, number);
    }

    /*
    Можно было использовать  DeleteMapping ,
    Для удобства тестирования оставил GetMapping
    */
    @RequestMapping(value = "/remove/{name}")
    @GetMapping
    public ResponseEntity removeCounter(@PathVariable(value = "name") String cntrName) {
        return _mainService.removeCounter(cntrName);
    }

    @RequestMapping(value = "/sum")
    @GetMapping
    public ResponseEntity sumCounters() {
        return _mainService.summCounters();
    }

    @RequestMapping(value = "/list")
    @GetMapping
    public ResponseEntity listCounters() {
        return _mainService.listCounters();
    }

    @RequestMapping(value = "/full")
    @GetMapping
    public ResponseEntity fullCounters() {
        return _mainService.fullCounters();
    }


}
