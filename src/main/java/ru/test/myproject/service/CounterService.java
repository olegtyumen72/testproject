package ru.test.myproject.service;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CounterService {

    ResponseEntity createCounter(String name);

    ResponseEntity addToCounter(String name, Integer value);

    ResponseEntity addToCounter(String name);

    ResponseEntity removeCounter(String name);

    ResponseEntity summCounters();

    ResponseEntity listCounters();

    ResponseEntity valueCounter(String name);

    ResponseEntity fullCounters();

}
