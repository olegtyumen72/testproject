package ru.test.myproject.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.test.myproject.config.CounterServiceConfig;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

@Service
@EnableConfigurationProperties(CounterServiceConfig.class)
public class CounterServiceImpl implements CounterService {

    private ObjectMapper _mapper;
    /*
        Для хранения в памяти можно исользовать альтернативы, например, Java Caching System или Ehcache.
        Учитывая, что подробного описания данных нет, я решил использовать простой и оптимальный вариант
     */
    private Map<String, Integer> _counters;
//    private Logger _logger = Logger.getLogger(CounterServiceImpl.class.getSimpleName());

    @Autowired
    public CounterServiceImpl(ObjectMapper mapper, CounterServiceConfig configs) {
        _mapper = mapper;
        Path main = Paths.get(configs.getFilePath()).toAbsolutePath();
        File saved = main.toFile();
        try {
            if (saved.exists()) {
                _counters = mapper.readValue(saved, new TypeReference<HashMap<String, Integer>>() {
                });
            } else {
                _counters = new HashMap();
            }
        } catch (Exception e) {
            saved.renameTo(Paths.get(main.getParent().toString(), UUID.randomUUID() + "_wrong.json").toFile());
            _counters = new HashMap();
        }
        Runtime.getRuntime().addShutdownHook(new Thread(() -> SaveOnClose(configs.getFilePath())));
    }

    /*
    В описание задания не было указано, нужно ли хранить инфу по завершению процесса.
    Реализовал на всякий случай, так как уточнить не мог.
     */
    private void SaveOnClose(String path) {
        try {
            File saved = Paths.get(path).toAbsolutePath().toFile();
            saved.getParentFile().mkdir();
            saved.createNewFile();
            FileWriter writer = new FileWriter(saved);
            writer.write(_mapper.writeValueAsString(_counters));
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ResponseEntity safeRun(Supplier<ResponseEntity> act) {
        /*
        Декоратор над выполняющейся функцией.
        С его помощью можно удобно логировать всё что угодно
         */
        try {
            return act.get();
        } catch (Exception e) {
//            _logger.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
            return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity createCounter(String name) {
        return safeRun(() -> {
            if (_counters.containsKey(name))
                return new ResponseEntity("Counter already exists", HttpStatus.CONFLICT);
            else {
                Integer res = _counters.put(name, 0);
                return new ResponseEntity(res == null ? 0 : res, HttpStatus.OK);
            }
        });
    }

    public ResponseEntity addToCounter(String name, Integer value) {
        return safeRun(() -> {
            /*
            Для того чтобы понять, что что-то пошло не так можно кидать ошибку
            либо выводить в сообщение ResponseEntity информацию о том, что не существует
            такого счётчика
             */
            if (_counters.containsKey(name))
                return new ResponseEntity(_counters.merge(name, value, Integer::sum), HttpStatus.OK);
            else
                return new ResponseEntity("Miss counter", HttpStatus.EXPECTATION_FAILED);
        });
    }

    @Override
    public ResponseEntity addToCounter(String name) {
        return addToCounter(name, 1);
    }

    public ResponseEntity removeCounter(String name) {
        return safeRun(() -> {
            if (_counters.containsKey(name)) {
                return new ResponseEntity(_counters.remove(name), HttpStatus.OK);
            } else {
                return new ResponseEntity("Miss counter", HttpStatus.EXPECTATION_FAILED);
            }
        });
    }

    public ResponseEntity summCounters() {
        return safeRun(() -> new ResponseEntity(_counters.values().stream().reduce(0, Integer::sum), HttpStatus.OK));
    }

    public ResponseEntity listCounters() {
        return safeRun(() -> new ResponseEntity(_counters.keySet(), HttpStatus.OK));
    }

    /**
     * Дебаг метод, возвращает все элементы
     *
     * @return HashMap
     */
    public ResponseEntity fullCounters() {
        return safeRun(() -> new ResponseEntity(_counters, HttpStatus.OK));
    }

    public ResponseEntity valueCounter(String name) {
        return safeRun(() -> {
            if (_counters.containsKey(name)) {
                return new ResponseEntity(_counters.get(name), HttpStatus.OK);
            } else {
                return new ResponseEntity("Miss counter", HttpStatus.EXPECTATION_FAILED);
            }
        });
    }

}
